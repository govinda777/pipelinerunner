﻿using PipelineRunner.Stages;
using System;
using System.Collections.Generic;
using PipelineRunner.Jobs;
using PipelineRunner.Stages;
using System;
using System.Collections.Generic;

namespace PipelineRunner.ConsoleApp
{
    class Program
    {
        class ParseFromString : AsyncJob<Request, Request>
        {
            public override bool InCaseOfErrorContinueOn()
            {
                return false;
            }

            public override Request Perform(Request param)
            {
                param.SuccessParseFromString = true;

                Console.WriteLine($"=== {this.ToString()} Success : {param.SuccessParseFromString} Content : {param.Content}");

                return param;
            }
            public override string ToString()
            {
                return "ParseFromString";
            }
        }

        class DivideByPI : AsyncJob<Request, Request>
        {
            public static int index = 0;

            public override bool InCaseOfErrorContinueOn()
            {
                return true;
            }

            public override Request Perform(Request param)
            {
                param.SuccessDivideByPI = false;

                index++;

                Console.WriteLine($"=== {this.ToString()} Success : {param.SuccessDivideByPI} Content : {param.Content}");

                return param;
            }
            public override string ToString()
            {
                return "DivideByPI";
            }
        }

        class Format : AsyncJob<Request, Request>
        {
            public override Request Perform(Request param)
            {
                Console.WriteLine($"=== {this.ToString()} Success : {param?.SuccessFormat} Content : {param?.Content}");

                return param;
            }
            public override string ToString()
            {
                return "Format";
            }
        }

        class Request
        {
            public bool SuccessParseFromString { get; set; }
            public bool SuccessDivideByPI { get; set; }
            public bool SuccessFormat { get; set; }
            public string Content { get; set; }
        }

        static void Main(string[] args)
        {
            var setup = Config
                        .Stage(new ParseFromString())
                        .Stage(new DivideByPI())
                        .Stage(new Format());

            var pipeline = Pipeline.Create(setup);

            var param = new List<Request>();

            for (int i = 0; i < 1; i++)
            {
                param.Add(new Request() { Content = $"Teste : {i}" });
            }

            pipeline.Run(param);

            Console.WriteLine("Hello World!");
        }
    }
}
