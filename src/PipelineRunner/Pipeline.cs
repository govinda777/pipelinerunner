﻿using PipelineRunner.Pipelines;
using PipelineRunner.Stages;
using System.Threading;

namespace PipelineRunner
{
    public static class Pipeline
    {
        /// <summary>
        /// Creates a new Pipeline.
        /// </summary>
        /// <returns></returns>
        public static IPipeline<TParam> Create<TParam, TResult>(StageSetup<TParam, TResult> stageSetup)
        {
            return new BasicPipeline<TParam>(stageSetup.ToEnumerable());
        }
    }
}
