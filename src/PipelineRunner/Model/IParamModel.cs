﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PipelineRunner.Model
{
    public interface IParamModel 
    {
        bool ProceedWithError();
    }
}
