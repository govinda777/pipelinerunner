﻿using PipelineRunner.Collections;
using System;

namespace PipelineRunner
{
    interface IPipelineJob<TParam, TResult>
    {
        TResult InternalPerform(TParam param);
        IQueue<TResult> Output { get; }
        bool PerformAsync { get; }
        bool InCaseOfErrorContinueOn();
    }

    interface IPipelineJob : IPipelineJob<object, object>
    {
    }
}
